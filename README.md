# wibble-react-gui

GUI for Wibble based on React. Mostly complete.

See YouTube for [Wibble CLI/GUI Teaser trailer](https://youtu.be/IpSRzhGrKu8), together with [Wibble GUI - Quick tour/overview](https://www.youtube.com/watch?v=LucjPjUv-Nc) for a summary overview.

## Dependencies

1. [Wibble CLI](https://codeberg.org/ArenTyr/wibble)
2. Working Npm/[Node.js](https://nodejs.org/) setup (also employs `npm serve`).
3. [Tmux](https://github.com/tmux/tmux/wiki) (strongly recommended for easy startup; also a great piece of software anyway).

Just about every GNU/Linux distribution will have Node.js and tmux in their package repositories.

## Installation/Quickstart

1. Install/setup Wibble CLI if you haven't already.
2. Create some notes (otherwise you'll have nothing to look at...!).
3. Head over to the [releases](https://codeberg.org/ArenTyr/wibble-react-gui/releases) page and download the latest build. Edit the `tmux-start-wibble-gui.sh` bash shell script to point to wherever you've extracted the build into, set your configured Wibble note data directory, and finally enter your browser command. Ensure that `wibble` is on your `PATH` so the script can find the command.
4. Run `tmux-start-wibble-gui.sh`. It should open your browser/tab and you're good to go. Watch documentation video for further guidance.

## Feature Summary

- GUI frontend for [Wibble CLI notes/wiki system](https://codeberg.org/ArenTyr/wibble)
- Search for any matching note(s) using multiple criteria
- View/preview markup notes with beautiful rendering (`md`, `org-mode`, `adoc`)
- View/preview source code notes with syntax highlighting
- View/show associated note data directory, linked files, and associated notes
- Quick open/edit buttons to easily edit/open note and directory on command line
- Rapidly render/view dozens of notes at a time
- Image rendering support provided in conjunction with `npm serve`
- View notes/wiki statistics, including tag breakdown, with nicely rendered charts
- Display a WebGL powered node/note graph, showing node/note connections (edges), colour-coded note categories
- Search and filter nodes/notes on the graph, immediately jump to nodes/notes from graph via ID link

## Screenshots

Launcher:

![](./img/wibble-gui-launcher-1000px.png)  

Complex search:

![](./img/wibble-gui-search-1000px.png)  

Note/Node Graph:

![](./img/wibble-gui-graph1-1000px.png)  
![](./img/wibble-gui-graph2-1000px.png)  

Note view:

![](./img/wibble-gui-noteview-1000px.png)  

Note view showing attachments/data directory listing:

![](./img/wibble-gui-noteview2-1000px.png)  

Syntax highlighting on source code files:

![](./img/wibble-gui-noteview3-1000px.png)  

Stats view:

![](./img/wibble-gui-stats-1000px.png)  
