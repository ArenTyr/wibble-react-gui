import './App.css';

import { BrowserRouter, Navigate, Routes, Route } from "react-router-dom";

import Launcher from "./pages/Launcher";
import NoteResults from "./pages/NoteResults";
import Search from "./pages/Search";
import Stats from "./pages/Stats";
import WibbleGraph from "./pages/WibbleGraph";

function App() {
  return (<BrowserRouter>
            <Routes>
                <Route path="" element={<Navigate replace to="/wibble" />} />
                <Route index element={<Launcher />} />
                <Route path="/wibble" element={<Launcher />} />
                <Route path="/wibble/graph" element={<WibbleGraph />} />
                <Route path="/wibble/search" element={<Search />} />
                <Route path="/wibble/stats" element={<Stats />} />
                <Route path="/wibble/search/:note_id" element={<Search />} />
                <Route path="/wibble/results" element={<NoteResults />} />
            </Routes>
          </BrowserRouter>);
}

export default App;
