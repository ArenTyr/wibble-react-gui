import { useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { useNavigate } from "react-router-dom";
import { useReducer } from 'react';

const formReducer = (state, event) => {
    return {
        ...state,
        [event.name]: event.value
    }
}

async function retrieveNoteData(queryPayload)
{
    const response = await fetch('http://127.0.0.1:22889', {
        headers: { "Content-Type": "application/json", "Accept": "application/json" },
        method: 'POST',
        mode: 'cors',
        body: JSON.stringify(queryPayload) });

    const note_results = await response.json();

    return note_results;
}

function pullNoteData(queryPayload)
{
    const note_results = retrieveNoteData(queryPayload);
    return note_results;
}

export default function SearchFormComponent() {

    const [formData, setFormData] = useReducer(formReducer, {});
    const navigate = useNavigate();
    const location = useLocation();
    //console.log(location);

    useEffect(() => {
    try {
        const noteIdMatch = location.pathname.match(/\/search\/(.*)/)[1];

        if(typeof noteIdMatch !== 'undefined') {
        let queryPayload = {
            "date":        "",
            "note_id":     noteIdMatch,
            "title":       "",
            "description": "",
            "filetype":    "",
            "project":     "",
            "tags":        "",
            "class":       "",
            "keypairs":    "",
            "custom":      ""
        }

        console.log("-> Initiating direct retrieval for note id: " + noteIdMatch);
        //console.log(queryPayload);
            pullNoteData(queryPayload).then(function displayResults(results) {
                console.log("DEBUG: Got results:");
                console.log(results);
                navigate('/wibble/results', {replace: true, state: { results }});
                return true;
                    });
        }
    }
    catch(err) {
        //console.log("-> No direct note ID specified, showing main search form.");
    }
    }, [location, navigate]);

    //const note_id_param = this.props.params.note_id;
    //console.log("GOT ID: " + note_id_param);

    function runQuery(formData) {
        //console.log("DEBUG: Submitted form data:");
        //console.log(formData);

        let queryPayload = {
            "date":        formData.Date,
            "note_id":     formData.NoteId,
            "title":       formData.Title,
            "description": formData.Description,
            "filetype":    formData.Filetype,
            "project":     formData.Project,
            "tags":        formData.Tags,
            "class":       formData.Class,
            "keypairs":    formData.KeyPairs,
            "custom":      formData.Custom
        }

        //console.log("DEBUG: Built query:");
        console.log(queryPayload);
        pullNoteData(queryPayload).then(function displayResults(results) {
            //console.log("DEBUG: Got results:");
            //console.log(results);
            navigate('/wibble/results', {replace: false, state: { results }});
            return true;
        });
    }

    const handleSubmit = event => {
        event.preventDefault();
        console.log(formData);
        document.getElementById("main_search_button").innerHTML = "Searching, please wait...";
        document.getElementById("main_search_button").disabled = true;
        runQuery(formData);
        
    }

    const handleChange = event => {

        setFormData({
            name: event.target.name,
            value: event.target.value,
        });
    }

    return (
        <div className="wrapper">
            <div className="note-form">
                <form className="the-form" onSubmit={handleSubmit}>
                    <h1>Search</h1>
                    <h3>Find Notes</h3>
                    <span className="wibble_navigation">
                    <a href='/wibble'><img alt="menu" src='/img/wibble-menu-icon.svg' height='50px'/></a>
                    <a href='/wibble/graph'><img alt="graph" src='/img/wibble-node-icon.svg' height='50px'/></a>
                    <a href='/wibble/stats'><img alt="stats" src='/img/wibble-graph-icon.svg' height='50px'/></a>
                    </span>
                    <div className="input-labels">
                        <div className="wibble-search-labels">
                            <div><label>Date:       </label></div>
                            <div><label>Note Id:    </label></div>
                            <div><label>Title:      </label></div>
                            <div><label>Description:</label></div>
                            <div><label>Filetype:   </label></div>
                            <div><label>Project:    </label></div>
                            <div><label>Tags:       </label></div>
                            <div><label>Class:      </label></div>
                            <div><label>KeyPairs:   </label></div>
                            <div><label>Custom:     </label></div>
                        </div>
                    </div>
                    <div className="wibble-search-input-boxes">
                        <div><input name="Date"        onChange={handleChange} /></div>
                        <div><input name="NoteId"      onChange={handleChange} /></div>
                        <div><input name="Title"       onChange={handleChange} /></div>
                        <div><input name="Description" onChange={handleChange} /></div>
                        <div><input name="Filetype"    onChange={handleChange} /></div>
                        <div><input name="Project"     onChange={handleChange} /></div>
                        <div><input name="Tags"        onChange={handleChange} /></div>
                        <div><input name="Class"       onChange={handleChange} /></div>
                        <div><input name="KeyPairs"    onChange={handleChange} /></div>
                        <div><input name="Custom"      onChange={handleChange} /></div>
                    </div>
                    <br></br>
                    <div className="clear"></div>
                    <div><button id="main_search_button" className="search_button" type="submit">Submit</button></div>
                </form>
            </div>
        </div>
    )
}

export { SearchFormComponent };
