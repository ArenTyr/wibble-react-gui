import { renderToStaticMarkup } from 'react-dom/server';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { atomOneDark } from 'react-syntax-highlighter/dist/esm/styles/hljs';

// For parsing plain text Wibble files
import * as DOMPurify from 'dompurify';
import parse from 'html-react-parser';
// For parsing org-mode formatted Wibble files
import Org from 'org';
// For parsing asciidoc formatted Wibble files
import asciidoctor from 'asciidoctor';
// For parsing markdown formatted Wibble files
import Markdown from 'react-markdown';
import remarkGfm from 'remark-gfm';

const imgReplace = /<img src="\/home\/aren\/doc\/wibble\/note_data\//g;
const fileReplace = /<img src="file:\/home\/aren\/doc\/wibble\/note_data\//g;
const fStub = "<br/><img class=\"displayed\" src=\"http://localhost:22890/";

const codeSyntax = (markup, codetype) => {
    return (
            <SyntaxHighlighter language={codetype} style={atomOneDark} useInlineStyles="false">
            {markup}
        </SyntaxHighlighter>
    );
};

const preProcessResults = (results) => {
    //const srcList = ["java", "c", "c++", "cpp", "pl", "pm", "sh", "cls"];
    // see https://raw.githubusercontent.com/highlightjs/highlight.js/main/SUPPORTED_LANGUAGES.md
    const srcList = ["1c", "4d", "sap-abap", "abap", "abnf", "accesslog", "ada", "apex", "armasm", "arm", "avrasm", "actionscript", "as", "alan", "i", "ln", "angelscript", "asc", "apache", "apacheconf", "applescript", "osascript", "arcade", "asciidoc", "aspectj", "autohotkey", "autoit", "awk", "mawk", "nawk", "gawk", "ballerina", "bal", "bash", "sh", "zsh", "basic", "bbcode", "blade", "bnf", "bqn", "brainfuck", "bf", "csharp", "cs", "c", "h", "cpp", "hpp", "cc", "hh", "c++", "h++", "cxx", "hxx", "cal", "c3", "cos", "cls", "candid", "did", "cmake", "cmake.in", "cobol", "standard-cobol", "codeowners", "coq", "csp", "css", "capnproto", "capnp", "chaos", "kaos", "chapel", "chpl", "cisco", "clojure", "clj", "coffeescript", "coffee", "cson", "iced", "cpc", "crmsh", "crm", "pcmk", "crystal", "cr", "curl", "cypher", "d", "dafny", "dart", "dpr", "dfm", "pas", "pascal", "diff", "patch", "django", "jinja", "dns", "zone", "bind", "dockerfile", "docker", "dos", "bat", "cmd", "dsconfig", "dts", "dust", "dst", "dylan", "ebnf", "elixir", "elm", "erlang", "erl", "excel", "xls", "xlsx", "extempore", "xtlang", "xtm", "fsharp", "fs", "fsx", "fsi", "fsscript", "fix", "flix", "fortran", "f90", "f95", "func", "gcode", "nc", "gams", "gms", "gauss", "gss", "godot", "gdscript", "gherkin", "hbs", "glimmer", "html.hbs", "html.handlebars", "htmlbars", "gn", "gni", "go", "golang", "gf", "golo", "gololang", "gradle", "graphql", "groovy", "gsql", "xml", "html", "xhtml", "rss", "atom", "xjb", "xsd", "xsl", "plist", "svg", "http", "https", "haml", "handlebars", "hbs", "html.hbs", "html.handlebars", "haskell", "hs", "haxe", "hx", "| hlsl", "hy", "hylang", "ini", "toml", "inform7", "i7", "irpf90", "iptables", "json", "java", "jsp", "javascript", "js", "jsx", "jolie", "iol", "ol", "julia", "jl", "julia-repl", "kotlin", "kt", "", "tex", "leaf", "lean", "lasso", "ls", "lassoscript", "less", "ldif", "lisp", "livecodeserver", "livescript", "ls", "lookml", "lua", "luau", "macaulay2", "makefile", "mk", "mak", "make", "markdown", "mkdown", "mkd", "mathematica", "mma", "wl", "matlab", "maxima", "mel", "mercury", "mips", "mipsasm", "mint", "mirc", "mrc", "mizar", "mkb", "mlir", "mojolicious", "monkey", "moonscript", "moon", "motoko", "mo", "n1ql", "nsis", "never", "nginx", "nginxconf", "nim", "nimrod", "nix", "oak", "", "ocaml", "ml", "objectivec", "mm", "objc", "obj-c", "obj-c++", "objective-c++", "glsl", "openscad", "scad", "ruleslanguage", "oxygene", "pf", "pf.conf", "php", "papyrus", "psc", "parser3", "perl", "pl", "pm", "pine", "pinescript", "plaintext", "text", "pony", "pgsql", "postgres", "postgresql", "powershell", "ps", "ps1", "processing", "prolog", "properties", "proto", "protobuf", "puppet", "pp", "python", "py", "gyp", "profile", "python-repl", "pycon", "qsharp", "k", "kdb", "qml", "r", "cshtml", "razor", "razor-cshtml", "reasonml", "re", "redbol", "rebol", "red", "red-system", "rib", "rsl", "rescript", "res", "risc", "riscript", "riscv", "riscvasm", "graph", "instances", "robot", "rf", "rpm-specfile", "rpm", "spec", "rpm-spec", "specfile", "ruby", "rb", "gemspec", "podspec", "thor", "irb", "rust", "rs", "rvt", "rvt-script", "SAS", "sas", "scss", "sql", "p21", "step", "stp", "scala", "scheme", "scilab", "sci", "sfz", "shexc", "shell", "console", "smali", "smalltalk", "st", "sml", "ml", "solidity", "sol", "spl", "stan", "stanfuncs", "stata", "iecst", "scl", "stl", "structured-text", "stylus", "styl", "subunit", "supercollider", "sc", "svelte", "swift", "tcl", "tk", "terraform", "tf", "hcl", "tap", "thrift", "toit", "tp", "tsql", "twig", "craftcms", "typescript", "ts", "tsx", "mts", "cts", "unicorn-rails-log", "vbnet", "vb", "vba", "vbscript", "vbs", "vhdl", "vala", "verilog", "v", "vim", "xsharp", "xs", "prg", "axapta", "x++", "x86asm", "x86asmatt", "xl", "tao", "xquery", "xpath", "xq", "xqm", "yml", "yaml", "zenscript", "zs", "zephir", "zep"];

    const txtList = ["txt", "log", "wiki"];
    const orgFiles = results.filter(results => results.metadata.type === "org");
    const mdFiles = results.filter(results => results.metadata.type === "md");
    const adocFiles = results.filter(results => results.metadata.type === "adoc");
    const srcFiles = results.filter(matches => srcList.includes(matches.metadata.type));
    const txtFiles = results.filter(matches => txtList.includes(matches.metadata.type));

    console.log("-> In pre-processor:");
    console.log("org");
    console.log(orgFiles);
    console.log("md");
    console.log(mdFiles);
    console.log("adoc");
    console.log(adocFiles);
    console.log("src");
    console.log(srcFiles);
    console.log("txt");
    console.log(txtFiles);
    return ([orgFiles, mdFiles, adocFiles, srcFiles, txtFiles]);
}

const navHeader = () => {
    return "<div class='nav_section'><span class='wibble_navigation'>"
        +  "<a href='/wibble'><img alt='menu' src='/img/wibble-menu-icon.svg' height='50px'/></a>"
        +  "<a href='/wibble/search'><img src='/img/wibble-search-loupe.svg' height='50px'/></a>"
        +  "<a href='/wibble/graph'><img src='/img/wibble-node-icon.svg' height='50px'/></a>"
        +  "<a href='/wibble/stats'><img src='/img/wibble-graph-icon.svg' height='50px'/></a></span></div>";
}

const buildNoteHeader = (noteHeader) => {
    console.log("-> Generating note HTML header body.");
    var header = '<div class="note"><div class="note_header"><hr><table>';
    header += "<tr><th>Date</th><td>" + noteHeader.date + "</td></tr>";
    header += "<tr><th>NoteId</th><td>" + noteHeader.note_id + "</td></tr>";
    header += "<tr><th>Title</th><td>" + noteHeader.title + "</td></tr>";
    header += "<tr><th>Description</th><td>" + noteHeader.desc + "</td></tr>";
    header += "<tr><th>Filetype</th><td>" + noteHeader.type + "</td></tr>";
    header += "<tr><th>Project</th><td>" + noteHeader.proj + "</td></tr>";
    header += "<tr><th>Tags</th><td>" + noteHeader.tags + "</td></tr>";
    header += "<tr><th>Class</th><td>" + noteHeader.class + "</td></tr>";
    header += "<tr><th>Keypairs</th><td>" + noteHeader.kpairs + "</td></tr>";
    header += "<tr><th>Custom</th><td>" + noteHeader.custom + "</td></tr>";
    header += "<tr><th>DataDirs</th><td>" + noteHeader.ddirs + "</td></tr>";
    header += '</table><hr></div>';
    console.log("-> Generated HTML header.");
    return header;
}

const handleClick = event => {
    try
    {
        const buttonData = event.target.getAttribute("data-param");
        const eventId = event.target.getAttribute("data-id");
        console.log(event.target);
        console.log("got eventId: " + eventId);
        const statusArea = document.getElementById("note_btn_status_" + eventId);

        // store data on clipboard
        navigator.clipboard.writeText(buttonData);
        statusArea.innerHTML = "<p>Copied to clipboard!</p>"; 
        setTimeout(function () {
            statusArea.innerHTML = "<p>&nbsp;</p>";
        }, 1000);
    }
    catch {}
}

const buildNoteToolbar = (note_id, note_fp, note_dd, note_content) => {

    if(typeof note_id === 'undefined') { return (<></>); }
    return (
            <div className="toolbar_container">
            <div className="note_toolbar">
            <button  id={"copyNoteIdButton_" + note_id} data-param={"wibble search -GE --id " + note_id}  data-id={note_id} className="toolbar_button" onClick={() =>
                handleClick()} type="button"><img alt="" src="/img/clipboard-id.svg" height="20px"/>Copy Wibble Edit Command</button>
            <button  id={"copyNoteFilePathButton_" + note_id} data-param={note_fp} data-id={note_id} className="toolbar_button" onClick={() =>
                handleClick()} type="button"><img alt="" src="/img/clipboard-path.svg" height="20px"/>Copy Note File Path</button>
            <button  id={"copyNoteDataDirButton_" + note_id } data-param={note_dd}  data-id={note_id} className="toolbar_button" onClick={() =>
                handleClick()} type="button"><img alt="" src="/img/clipboard-dd.svg" height="20px"/>Copy Note Data Directory Path</button>
            <button  id={"copyNoteContentButton_" + note_id} data-param={note_content}  data-id={note_id} className="toolbar_button" onClick={() =>
                handleClick()} type="button"><img alt="" src="/img/clipboard-content.svg" height="20px"/>Copy Note Content</button>
            </div><div id={"note_btn_status_" + note_id} className="button_click_status"><p>&nbsp;</p></div><hr/></div>);
}

const buildRelatedNotes = (noteRelated) => {
    var relatedNotes = "";

    if(noteRelated.length === 0 || noteRelated === "0") { return relatedNotes; }
    relatedNotes = "<div class='related_notes'>";
    var parentNotes = "<div><h4>Parent Notes</h4><ul>";
    var siblingNotes = "<div><h4>Sibling Notes</h4><ul>";
    var childNotes = "<div><h4>Child Notes</h4><ul>";

    try {
        noteRelated.forEach(entry => {
            var prunedTitle = "";
            //console.log("ENTRY object: %o", entry);
            if (entry.rel === "P") {
                prunedTitle = entry.title;
                if(entry.title.length > 60) { prunedTitle = entry.title.substring(0, 60) + "..."; }
                parentNotes += "<li><a href='http://localhost:3000/wibble/search/" +
                               entry.id + "'>" + prunedTitle + " [" + entry.type + "] </a></li>";
            }
            else if(entry.rel === "S") {
                prunedTitle = entry.title;
                if(entry.title.length > 60) { prunedTitle = entry.title.substring(0, 60) + "..."; }
                siblingNotes += "<li><a href='http://localhost:3000/wibble/search/" +
                                entry.id + "'>" + prunedTitle + " [" + entry.type + "] </a></li>";
            }
            else if(entry.rel === "C") {
                prunedTitle = entry.title;
                if(entry.title.length > 60) { prunedTitle = entry.title.substring(0, 60) + "..."; }
                childNotes += "<li><a href='http://localhost:3000/wibble/search/" +
                              entry.id + "'>" + prunedTitle + " [" + entry.type + "] </a></li>";
            }
        });
        parentNotes += "</ul></div>";
        siblingNotes += "</ul></div>";
        childNotes += "</ul></div>";

        relatedNotes += parentNotes + siblingNotes + childNotes + "</div>";
    }
    catch(err)
    {
        console.log("-> Error processing related notes: " + err);
    }

    return relatedNotes;
}

const processLinkedFiles = (lfiles) => {
    if(lfiles === "___NOT_FOUND___") { return ""; }
    //console.log(lfiles);
    console.log("-> Linked files found, returning.");
    return '<div class="content"><h2>Linked Files</h2>' + lfiles + '</div><br/>';
}

const processDataDir = (datadir, dataTree) => {
    if(datadir === "___NOT_FOUND___" || dataTree === "___NOT_FOUND___") { return "</div></div>"; }
    //console.log(datadir);
    console.log("-> Data directory found, generating.");
    const dataDirListing = datadir.split("\"");
    const dataDirOpenDiv = '<div class="data_dir_listing"><div class="file_listing_content"><div class="note_file_listing"><h2>Data Directory</h2>';
    var dataDirBody = "<br/><hr/><br/><h3>File Listing</h3>";
    dataDirListing.forEach(line => {
        line = line.replaceAll(/\n.*/g, "");
        if     (line === ":")   {}
        else if(line === " " )  {}
        else if(line === ""   ) {}
        else if(line.charAt(0) === "/")
        {
            dataDirBody += "<br/><p><b>" + line + "</b></p><br/>";
        }
        else
        {
            dataDirBody += "<p> ➤ " + line + "</p>";
        }
    });

    var dataTreeBody = "<br/><hr/><br/><h3>File Tree</h3><br/>";
    const dataTreeListing = dataTree.split("\n");
    dataTreeListing.forEach(line => {
        line = line.replace(/\| {3}/g, "|.....");
        line = line.replace(/`--/g,   "|────‣");
        line = line.replace(/ {4}/g,  "|.....");
        line = line.replace(/\|-- /g, "|────‣");
        dataTreeBody += "<p>" + line + "</p>";
    });

    console.log("-> Data directory listing generated.");
    return dataDirOpenDiv + dataDirBody + dataTreeBody + '<br/><hr/><div></div></div><br/>';
}

const processOrgFileBody = (note) => {
    console.log("-> Generating ORG HTML body.");
    console.log("-> DEBUG: Parsing: " + note);
    var orgParser = new Org.Parser();
    var orgDocument = orgParser.parse(note);
    var orgHTMLDocument = '<div class="note_content">';
    orgHTMLDocument += orgDocument.convert(Org.ConverterHTML, {
        toc: 0,
        headerOffset: 1,
        exportFromLineNumber: false,
        suppressSubScriptHandling: false,
        suppressAutoLink: true
    }) + "</div>";

    var orgDoc = orgHTMLDocument.toString().replace(imgReplace, fStub);
    orgDoc = orgDoc.replace(fileReplace, fStub);
    orgDoc = orgDoc.replaceAll(/href="#header.*?"/g, "href='javascript:;' style='text-decoration: none; color: black;'");
    orgDoc = orgDoc.replaceAll(/<p>:ID:/g, "<p class='org-id'>:ID:");
    orgDoc = orgDoc.replaceAll(/:PROPERTIES:.*?:ID:(.*)?:END:/gm, "<p class='org-id'>:ID:$1:END</p>");
    console.log("-> Generated ORG HTML body.");
    return orgDoc;
}

const processOrgFile = (orgNote) => {
    console.log("-> Processing ORG file.");
    var orgHTML = "";
    orgHTML += buildNoteHeader(orgNote.metadata);
    orgHTML += renderToStaticMarkup(buildNoteToolbar(orgNote.metadata.note_id, orgNote.note_fp, orgNote.data_dir_path, orgNote.content));
    orgHTML += buildRelatedNotes(orgNote.metadata.linkids_full_map);
    orgHTML += processOrgFileBody(orgNote.content);
    orgHTML += processLinkedFiles(orgNote.linked_files);
    orgHTML += processDataDir(orgNote.data_dir, orgNote.data_dir_tree);
    orgHTML += "</div></div></div>";
    const cleanedHTML = DOMPurify.sanitize(orgHTML, { USE_PROFILES: { html: true } });
    console.log("-> Processed ORG file.");
    return cleanedHTML;
}

const processMdFileBody = (note) => {

    return (<Markdown remarkPlugins={[remarkGfm]}>{note}</Markdown>);
}

const processMdFile = (mdNote) => {
    console.log("-> Processing MD file.");
    var mdHTML = "";
    mdHTML += buildNoteHeader(mdNote.metadata);
    mdHTML += renderToStaticMarkup(buildNoteToolbar(mdNote.metadata.note_id, mdNote.note_fp, mdNote.data_dir_path, mdNote.content));
    mdHTML += buildRelatedNotes(mdNote.metadata.linkids_full_map);
    mdHTML += '<div class="note_content">';
    var rawmdHTML = renderToStaticMarkup(processMdFileBody(mdNote.content));
    rawmdHTML = rawmdHTML.replace(imgReplace, fStub);
    rawmdHTML = rawmdHTML.replace(fileReplace, fStub);
    mdHTML += rawmdHTML;
    mdHTML += processDataDir(mdNote.data_dir, mdNote.data_dir_tree);
    mdHTML += "</div></div></div></div>";
    const cleanedHTML = DOMPurify.sanitize(mdHTML, { USE_PROFILES: { html: true } });
    console.log("-> Processed MD file.");
    return cleanedHTML;
}

const processAdocFileBody = (note) => {
    console.log("-> Generating ADOC HTML body.");
    const adocParser = asciidoctor();
    var adocDocument = '<div class="note_content">';
    var rawadocHTML = adocParser.convert(note, { 'attributes': { 'showtitle': true, 'icons': 'font' } });
    rawadocHTML = rawadocHTML.replace(imgReplace, fStub);
    rawadocHTML = rawadocHTML.replace(fileReplace, fStub);
    rawadocHTML = rawadocHTML.replaceAll(/href="#_.*?"/g, "href='javascript:;' style='text-decoration: none; color: black;'");
    adocDocument += rawadocHTML;
    adocDocument += "</div>";
    console.log("-> Generated ORG ADOC body.");
    return adocDocument;
}

const processAdocFile = (adocNote) => {
    console.log("-> Processing ADOC file.");
    var adocHTML = "";
    adocHTML += buildNoteHeader(adocNote.metadata);
    adocHTML += renderToStaticMarkup(buildNoteToolbar(adocNote.metadata.note_id, adocNote.note_fp, adocNote.data_dir_path, adocNote.content));
    adocHTML += buildRelatedNotes(adocNote.metadata.linkids_full_map);
    adocHTML += processAdocFileBody(adocNote.content);
    adocHTML += processDataDir(adocNote.data_dir, adocNote.data_dir_tree);
    adocHTML += "</div></div></div>";
    const cleanedHTML = DOMPurify.sanitize(adocHTML, { USE_PROFILES: { html: true } });
    console.log("-> Processed ADOC file.");
    return cleanedHTML;
}

const processSrcFile = (srcNote) => {
    console.log("-> Processing SRC file.");
    var srcHTML = "";
    srcHTML += buildNoteHeader(srcNote.metadata);
    srcHTML += renderToStaticMarkup(buildNoteToolbar(srcNote.metadata.note_id, srcNote.note_fp, srcNote.data_dir_path, srcNote.content));
    srcHTML += buildRelatedNotes(srcNote.metadata.linkids_full_map);
    srcHTML += "<br/>";
    srcHTML += renderToStaticMarkup(codeSyntax(srcNote.content, srcNote.metadata.type)) + "<br/>";
    srcHTML += processDataDir(srcNote.data_dir, srcNote.data_dir_tree);
    srcHTML += "</div></div></div>";
    const cleanedHTML = DOMPurify.sanitize(srcHTML, { USE_PROFILES: { html: true } });
    console.log("-> Processed SRC file.");
    return cleanedHTML;
}

const processTxtFile = (txtNote) => {
    console.log("-> Processing TXT file.");
    var txtHTML = "";
    txtHTML += buildNoteHeader(txtNote.metadata);
    txtHTML += renderToStaticMarkup(buildNoteToolbar(txtNote.metadata.note_id, txtNote.note_fp, txtNote.data_dir_path, txtNote.content));
    txtHTML += buildRelatedNotes(txtNote.metadata.linkids_full_map);
    txtHTML += '<div class="note_content">';
    txtHTML += txtNote.content;
    txtHTML = txtHTML.replace(/\n/g, "<br/>");
    txtHTML += processDataDir(txtNote.data_dir, txtNote.data_dir_tree);
    txtHTML += "</div></div>";
    const cleanedHTML = DOMPurify.sanitize(txtHTML, { USE_PROFILES: { html: true } });
    console.log("-> Processed TXT file.");
    return cleanedHTML;
}

export { preProcessResults, processOrgFile, processMdFile,
         processAdocFile, processSrcFile, processTxtFile, handleClick,
         buildNoteToolbar, parse, navHeader };
