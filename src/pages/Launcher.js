
const Launcher = () => {

    const WibbleButtons = () => {

        return (
                <div className="wibble_launcher_controls">
                    <div className="wibble_launcher_text"><h1>Wibble Launcher</h1></div>
                    <div className="launcher_buttons">
                        <div className="wibble_launcher_button">
                            <a href="/wibble/search" className="wibble_a_button"><img src="/img/wibble-search-loupe.svg" height="200px" alt="Search"/></a>
                            <div className="wibble_button_text"><b>Search</b></div>
                        </div>
                        <div className="wibble_launcher_button">
                            <a href="/wibble/graph" className="wibble_a_button"><img src="/img/wibble-node-icon.svg" height="200px" alt="Search"/></a>
                            <div className="wibble_button_text"><b>View Graph</b></div>
                        </div>
                        <div className="wibble_launcher_button">
                            <a href="/wibble/stats" className="wibble_a_button"><img src="/img/wibble-graph-icon.svg" height="200px" alt="Search"/></a>
                            <div className="wibble_button_text"><b>View Stats</b></div>
                        </div>
                    </div>
                <br/>
                </div>);
    }

    return (<div className="launcher_page">
            <WibbleButtons/>
            </div>
           );
}

export default Launcher;
