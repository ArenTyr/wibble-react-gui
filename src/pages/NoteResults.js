import { useLocation } from 'react-router-dom';
import { useEffect } from 'react';

import { preProcessResults, processOrgFile, processMdFile,
         processAdocFile, processSrcFile, processTxtFile, handleClick,
         parse, navHeader } from '../logic/NoteProcessingFunctions.js';

const NoteResults = () => {

    const location = useLocation();
    const results = location.state.results.matched_notes;

    var orgHTML  = [];
    var mdHTML   = [];
    var adocHTML = [];
    var srcHTML  = [];
    var txtHTML  = [];


    if(typeof results !== 'undefined')
    {
        const noteGroupArray = preProcessResults(results);
        //console.log("DEBUG: noteGroupArray", noteGroupArray);

        // 1. [0] org-mode files (orgFiles)
        // 2. [1] markdown files (mdFiles)
        // 3. [2] asciidoc files (adocFiles)
        // 4. [3] source files (srcFiles)
        // 5. [4] plain text/all other files (txtFiles)
        noteGroupArray[0].forEach(note => { orgHTML.push(processOrgFile(note));   });
        noteGroupArray[1].forEach(note => { mdHTML.push(processMdFile(note));     });
        noteGroupArray[2].forEach(note => { adocHTML.push(processAdocFile(note)); });
        noteGroupArray[3].forEach(note => { srcHTML.push(processSrcFile(note));   });
        noteGroupArray[4].forEach(note => { txtHTML.push(processTxtFile(note));   });

        console.log("-> Note processing complete. RENDERING.");
    }
    else
    {
        console.log("-> WARNING. Empty result set. RENDERING.");

    }

    // Add the event listeners to the DOM for the toolbar buttons which were added via static markup
    useEffect(() => {
        var noteIdButton;
        var noteFilePathButton;
        var noteDataDirButton;
        var noteContentButton;

        results.forEach( match => {
            try { 
                    noteIdButton       = document.getElementById("copyNoteIdButton_"       + match.metadata.note_id);
                    noteFilePathButton = document.getElementById("copyNoteFilePathButton_" + match.metadata.note_id);
                    noteDataDirButton  = document.getElementById("copyNoteDataDirButton_"  + match.metadata.note_id);
                    noteContentButton  = document.getElementById("copyNoteContentButton_"  + match.metadata.note_id);
            }
            catch {}

            if(noteIdButton !== null && noteIdButton !== undefined )             { noteIdButton.addEventListener("click", handleClick);       }
            if(noteFilePathButton !== null && noteFilePathButton !== undefined ) { noteFilePathButton.addEventListener("click", handleClick); }
            if(noteDataDirButton !== null && noteDataDirButton !== undefined )   { noteDataDirButton.addEventListener("click", handleClick);  }
            if(noteContentButton !== null && noteContentButton !== undefined )   { noteContentButton.addEventListener("click", handleClick);  }
            });


        // cleanup
        return () => {
            results.forEach( match => {

                try { 
                        noteIdButton       = document.getElementById("copyNoteIdButton_"       + match.metadata.note_id);
                        noteFilePathButton = document.getElementById("copyNoteFilePathButton_" + match.metadata.note_id);
                        noteDataDirButton  = document.getElementById("copyNoteDataDirButton_"  + match.metadata.note_id);
                        noteContentButton  = document.getElementById("copyNoteContentButton_"  + match.metadata.note_id);
                }
                catch {}

                if(noteIdButton !== null && noteIdButton !== undefined )             { noteIdButton.removeEventListener("click", handleClick);       }
                if(noteFilePathButton !== null && noteFilePathButton !== undefined ) { noteFilePathButton.removeEventListener("click", handleClick); }
                if(noteDataDirButton !== null && noteDataDirButton !== undefined )   { noteDataDirButton.removeEventListener("click", handleClick);  }
                if(noteContentButton !== null && noteContentButton !== undefined )   { noteContentButton.removeEventListener("click", handleClick);  }
                });
        }

        
    }, [results]);                                                                                  

    if(orgHTML.length === 0 && mdHTML.length === 0 && adocHTML.length === 0 && srcHTML.length === 0 && txtHTML.length === 0)
    {
        return (<div className="note_results">
                <div className="empty_results">
                <h2> No matched notes</h2>
                <h3>Start a <a href='/wibble'>new search</a></h3>
                </div>
                </div>);
                
    }
    else
    {
    return (<div className="note_results"> { parse(navHeader())       }
            <div className="org_results">  { parse(orgHTML.join(""))  } </div>
            <div className="md_results">   { parse(mdHTML.join(""))   } </div>
            <div className="adoc_results"> { parse(adocHTML.join("")) } </div>
            <div className="src_results">  { parse(srcHTML.join(""))  } </div>
            <div className="txt_results">  { parse(txtHTML.join(""))  } </div>
                                           { parse(navHeader())       } </div>);
    }
};

export default NoteResults;
