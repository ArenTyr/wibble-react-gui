import { useEffect } from "react";
import { useState } from "react";
import React from 'react';
import {
    ArcElement,
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';

import { Bar } from 'react-chartjs-2';
import { Doughnut } from 'react-chartjs-2';

ChartJS.register(
    ArcElement,
    CategoryScale,
    LinearScale,
    BarElement,
    Title,
    Tooltip,
    Legend
);

export const options = {
  responsive: true,
  plugins: {
    legend: {
        position: 'top',
        display: false
    },
    title: {
      display: false,
      text: 'Wibble Statistics',
    },
  },
};


        async function noteStatsData() {

            try {
                const queryPayload = { "mode": "statistics" };
                const response = await fetch('http://127.0.0.1:22889', {
                    headers: { "Content-Type": "application/json", "Accept": "application/json" },
                    method: 'POST',
                    mode: 'cors',
                    body: JSON.stringify(queryPayload)
                });

                if (response.ok) {
                    const statsData = await response.json();
                    return statsData;
                }
            }
            catch (error) {
                console.log("ERROR: " + error);
                const empty = { "wibble_data": [] };
                return empty;
            }
        }

        async function getTheStatsData() {
            const statsData = await noteStatsData();
            return statsData;
        }

/* generate random colour set for charts 
https://stackoverflow.com/questions/45771849/chartjs-random-colors-for-each-part-of-pie-chart-with-data-dynamically-from-data
*/

const getChartColourSet = (dataSeries) => {
    let randomBackgroundColorSet = [];
    let usedColors = new Set();

    let dynamicColors = function () {
        let r = Math.floor(Math.random() * 255);
        let g = Math.floor(Math.random() * 255);
        let b = Math.floor(Math.random() * 255);
        let color = "rgb(" + r + "," + g + "," + b + ")";

        if (!usedColors.has(color)) {
            usedColors.add(color);
            return color;
        } else {
            return dynamicColors();
        }
    };

    dataSeries.forEach(() => {randomBackgroundColorSet.push(dynamicColors()); });
    return randomBackgroundColorSet;
}

function StatsPanel({ stats: {ark_note_storage, main_note_storage, note_data_storage, base_directory, main_db, ark_db}}) {

    return (<div className="stats_display_pane"><h2>Storage Statistics</h2>
            <div className="node_info_details_pane">
            <p><span className="field"><b>Main Note Storage:    </b></span><span className="field_value">{ark_note_storage}</span></p>
            <p><span className="field"><b>Archived Note Storage:</b></span><span className="field_value">{main_note_storage}</span></p>
            <p><span className="field"><b>Note Data Storage:</b></span><span className="field_value">{note_data_storage}</span></p>
            <p><span className="field"><b>Base Directory:</b></span><span className="field_value">{base_directory}</span></p>
            <p><span className="field"><b>Main DB Location:</b></span><span className="field_value">{main_db}</span></p>
            <p><span className="field"><b>Archived DB Location:</b></span><span className="field_value">{ark_db}</span></p>
            </div>
            </div>);
};

const HeaderSection = () => {
    return (<div className="stats_header">
            <h1>Statistics</h1>
            <span className="wibble_navigation">
            <a href='/wibble'><img alt="menu" src='/img/wibble-menu-icon.svg' height='50px'/></a>
            <a href='/wibble/search'><img alt="graph" src='/img/wibble-search-loupe.svg' height='50px'/></a>
            <a href='/wibble/graph'><img alt="graph" src='/img/wibble-node-icon.svg' height='50px'/></a>
            </span>
            </div>);
}

const WibbleStats = () => {

    const [tagData, setTagData] = useState(null); //useState(obtainChartData());
    const [fileCountData, setFileCountData] = useState(null); //useState(obtainChartData());
    const [statsPanelData, setStatsPanelData] = useState(null); //useState(obtainChartData());

    const getAllTheChartData = async () => {
        try {
            const statsData = await getTheStatsData();
            const tagSeries = statsData.wibble_data.tag_data.tag_labels;
            const fileSeries = statsData.wibble_data.file_count_labels;

            console.log("STATS DATA: %o", statsData);
            setTagData({
                labels: statsData.wibble_data.tag_data.tag_labels,
                datasets: [
                    {
                        label: 'Tag Statistics',
                        data: statsData.wibble_data.tag_data.tag_counts,
                        backgroundColor: getChartColourSet(tagSeries)
                    },
                ],
            });

            setFileCountData({
                    labels: statsData.wibble_data.file_count_labels,
                    datasets: [
                        {
                            label: 'File Count Statistics',
                            data: statsData.wibble_data.file_count_nums,
                            backgroundColor: getChartColourSet(fileSeries)
                        },
                    ],
                });

            setStatsPanelData({stats: statsData.wibble_data.storage_stats});


            
        }
        catch (err) {
            console.log(err);
        }
    };

    useEffect(() => {

        getAllTheChartData();
    }, []);


    console.log("TAG DATA: %o", tagData);
    return (<div><div style={{ background: "white" }}>
            {
                (!tagData || !fileCountData) ? (<span>loading...</span>) :
            (<div className="graph_content_area">
                 <div className="stats_display_container">
                 <div className="stats_graph_panel">
                    <HeaderSection/>
                    <StatsPanel {...statsPanelData} />
                    <br/>
                    <h2>File Count</h2>
                    <Bar options={options} data={fileCountData} />
                    <br/>
                    <h2>Tag Breakdown</h2>
                    <Bar options={options} data={tagData} />
                    <br/>
                    <Doughnut options={options} data={tagData} />
                </div>
                </div>
             </div>
            )
            }
            </div>
             <div className="graph_bottom_spacer_bg"></div>
                </div>
        );
}

const StatsChart = () => {

    return (<div>
            <WibbleStats />
            </div>);
}

export default StatsChart;
