import { useEffect } from "react";
import { useState } from "react";
import { MultiDirectedGraph } from "graphology";
import { SigmaContainer, useLoadGraph, useRegisterEvents,
         ControlsContainer, ZoomControl, FullScreenControl,
         SearchControl } from "@react-sigma/core";
import "@react-sigma/core/lib/react-sigma.min.css";
import forceAtlas2 from "graphology-layout-forceatlas2";
import noverlap from 'graphology-layout-noverlap';

import parse from 'html-react-parser';

let alreadyRender = 0;

// used for testing only
//function makeid(length) {
//   var result           = '';
//   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
//   var charactersLength = characters.length;
//   for ( var i = 0; i < length; i++ ) {
//      result += characters.charAt(Math.floor(Math.random() * charactersLength));
//   }
//   return result;
//}

const retrieveNoteData = async() => {

    try {
        const queryPayload = { "mode": "metadata_only" };
        const response = await fetch('http://127.0.0.1:22889', {
            headers: { "Content-Type": "application/json", "Accept": "application/json" },
            method: 'POST',
            mode: 'cors',
            body: JSON.stringify(queryPayload) });

        if(response.ok) {
            const note_data = response.json();
            return note_data;
        }
    }
    catch(error)
    {
        console.log("ERROR: " + error);
        const empty = { "matched_notes": [] };
        return empty;
    }

}

const projColours = [ "#63A375",
                      "#F5CB5C",
                      "#870058",
                      "#3772FF",
                      "#A4303F",
                      "#DCED31",
                      "#9FBBCC",
                      "#19535F",
                      "#DE9151",
                      "#48304D",
                      "#23CE6B",
                      "#40798C",
                      "#FF6666",
                      "#F6F8FF",
                      "#00F5D4",
                      "#090C9B",
                      "#E6C0E9",
                      "#8B786D",
                      "#EBEFBF" ];

const getProjColour = () => {
    const colour = projColours[(Math.floor(Math.random() * projColours.length))];
    return colour;
}

let projectsColourMap = new Map();

const getStoreColourMap = (project) => {

    let projColour = projectsColourMap.get(project);
    if(projColour === undefined)  {
        projColour = getProjColour();
        projectsColourMap.set(project, projColour);
    }

    console.log("Returning projColour: ", projColour);
    return projColour;
}

const handleToolbarClick = event => {
    try
    {
        event.preventDefault();
        const buttonData = event.target.getAttribute("data-param");
        const statusArea = document.getElementById("note_btn_status");

        // store data on clipboard
        navigator.clipboard.writeText(buttonData);
        statusArea.innerHTML = "<p>Copied to clipboard!</p>"; 
        setTimeout(function () {
            statusArea.innerHTML = "<p>&nbsp;</p>";
        }, 1000);
    }
    catch { console.log("ERROR with handleToolbarclick"); }
}

export const LoadGraph = ({ panelData, setPanelData, colourMapList, setColourMapList }) => {

    const loadGraph = useLoadGraph();
    //const { positions, assign } = useLayout
    const registerEvents = useRegisterEvents();

    useEffect(() => {
        // upate the node information panel
        const updatePanelData = (date, title,  note_id,
                                 desc, ft,     proj,
                                 tags, classN, kp, cust,
                                 note_fp, data_dir_path) => {
                                     setPanelData(previousState => {
                                         return { 
                                             note_date: date,
                                             note_title: title,
                                             note_id: note_id,
                                             note_desc: desc,
                                             note_filetype: ft,
                                             note_proj: proj,
                                             note_tags: tags,
                                             note_class: classN,
                                             note_kp: kp,
                                             note_cust: cust,
                                             note_filepath: note_fp,
                                             note_dd: data_dir_path
                                         }
                                     });
                                 }                                  
        function showNodeDataOnClick (nodeClicked, nodeDataSet) {

            console.log("Got node ID:", nodeClicked);
            //console.log("Got %o", nodeDataSet);
            const matchedNode = nodeDataSet.filter(note => note.metadata.note_id === nodeClicked);

            if(matchedNode.length === 1) {
                const classN = matchedNode[0].metadata.class;
                const { metadata: { date, title, note_id, desc, type, proj, tags, kpairs, custom  } } = matchedNode[0]; 
                const { note_fp, data_dir_path } = matchedNode[0]; 

                updatePanelData(date, title, note_id, desc, type, proj, tags, classN, kpairs, custom, note_fp, data_dir_path);
            } 
        };

        // IMPORTANT: Do not re-render entire graph if we have already computed it:
        // pointless, slow, alters node positions on every click
        if(! alreadyRender) { 
            
            const graph = new MultiDirectedGraph();

            (async function() {

                const note_data = await retrieveNoteData();
                //console.log("DEBUG: Got %o", note_data);
                console.log("GENERATING...");
                let edgeList = [];
                note_data.matched_notes.forEach(note => {
                    //graph.addNode(makeid(10), { x: 0, y: 0, size: 5, label: note.metadata.title.substring(0,80), node_id: note.metadata.note_id}); 

                    let nodeType = "Unknown";
                    if(note.metadata.proj === "Computing") {
                        nodeType = "Computing";
                    }
                    else {
                        nodeType = "Not-Computing";
                    }
                    
                    const nodeColour = getStoreColourMap(note.metadata.proj);
                    graph.addNode(note.metadata.note_id, { x: 0, y: 0, size: 5, node_type: nodeType,
                                                           color: nodeColour, labelColor: "#A4303F", 
                                                           label: note.metadata.title.substring(0,80),
                                                           node_id: note.metadata.note_id}); 

                    // process related notes/edges
                    const relatedNotes = note.metadata.linkids.split(" ");
                    relatedNotes.forEach(entry => {
                        console.log("got related note: " + entry);
                        const relationship = entry.substring(0, 1);
                        const linkedNode = entry.substring(3);
                        console.log("got relationship: " + relationship);
                        console.log("got linkedNode: " + linkedNode);

                        // build edge specification to add later (once all nodes are on graph)
                        // S = Source, D = Destination
                        let newEdge = { "R": relationship,
                                        "S": note.metadata.note_id,
                                        "D": linkedNode
                                      };
                        edgeList.push(newEdge);
                    });
                    
                });


                const updateColourMapList = (projectsColourMap) => {
                    setColourMapList(previousState => { return projectsColourMap });
                }
                updateColourMapList(projectsColourMap);

                edgeList.forEach(edgeDef => {
                    if(edgeDef.R === "P") {
                        if(! graph.hasEdge(edgeDef.S + "_parent_of_" + edgeDef.D)) {
                            graph.addEdgeWithKey(edgeDef.S + "_parent_of_" + edgeDef.D, edgeDef.S, edgeDef.D,
                                                 { label: "Parent Of", color: "#FA4F40", weight: 10 });
                            console.log("-> Added PARENT edge.");
                        }
                        else { console.log("-> Skipping edge ", edgeDef.S); }
                    }
                    else if(edgeDef.R === "S") {
                        if(! graph.hasEdge(edgeDef.S + "_sibling_of_" + edgeDef.D)) {
                            graph.addEdgeWithKey(edgeDef.S + "_sibling_of_" + edgeDef.D, edgeDef.S, edgeDef.D,
                                                 { label: "Sibling Of", color: "#00ae04", weight: 8 });
                            console.log("-> Added SIBLING edge.");
                        }
                        else { console.log("-> Skipping edge ", edgeDef.S); }
                    }
                    else if(edgeDef.R === "C") {
                        if(! graph.hasEdge(edgeDef.S + "_child_of_" + edgeDef.D)) {
                            graph.addEdgeWithKey(edgeDef.S + "_child_of_" + edgeDef.D, edgeDef.S, edgeDef.D,
                                                 { label: "Child Of", color: "#ffac00", weight: 10 });
                            console.log("-> Added CHILD edge.");
                        }
                        else { console.log("-> Skipping edge ", edgeDef.S); }
                    }
                    else {
                        //console.log("No match for edgeList.R with value: ", edgeDef.R);
                    }

                });

                // seed initial positions
                noverlap.assign(graph);
                // apply ForceAtlas2 algorithm, prioritise nodes with edges
                forceAtlas2.assign(graph, { settings: { weighted: true, edgeWeightInfluence: 1, gravity: 2, scalingRatio: 5 }, iterations: 100 });            
                loadGraph(graph);

                console.log("Generation complete. Click to view generated graph.");

                registerEvents({
                    clickNode: (event) => showNodeDataOnClick(event.node, note_data.matched_notes)
                });
                /* ========== DEBUG: register events logging ============ */
                /*registerEvents({
                    // node events
                    //clickNode: (event) => console.log("clickNode", event.event, event.node, event.preventSigmaDefault),
                    clickNode: (event) => showNodeDataOnClick(event.node, note_data.matched_notes),
                    doubleClickNode: (event) => console.log("doubleClickNode", event.event, event.node, event.preventSigmaDefault),
                    rightClickNode: (event) => console.log("rightClickNode", event.event, event.node, event.preventSigmaDefault),
                    wheelNode: (event) => console.log("wheelNode", event.event, event.node, event.preventSigmaDefault),
                    downNode: (event) => console.log("downNode", event.event, event.node, event.preventSigmaDefault),
                    enterNode: (event) => console.log("enterNode", event.node),
                    leaveNode: (event) => console.log("leaveNode", event.node),
                    // edge events
                    clickEdge: (event) => console.log("clickEdge", event.event, event.edge, event.preventSigmaDefault),
                    doubleClickEdge: (event) => console.log("doubleClickEdge", event.event, event.edge, event.preventSigmaDefault),
                    rightClickEdge: (event) => console.log("rightClickEdge", event.event, event.edge, event.preventSigmaDefault),
                    wheelEdge: (event) => console.log("wheelEdge", event.event, event.edge, event.preventSigmaDefault),
                    downEdge: (event) => console.log("downEdge", event.event, event.edge, event.preventSigmaDefault),
                    enterEdge: (event) => console.log("enterEdge", event.edge),
                    leaveEdge: (event) => console.log("leaveEdge", event.edge),
                    // stage events
                    clickStage: (event) => console.log("clickStage", event.event, event.preventSigmaDefault),
                    doubleClickStage: (event) => console.log("doubleClickStage", event.event, event.preventSigmaDefault),
                    rightClickStage: (event) => console.log("rightClickStage", event.event, event.preventSigmaDefault),
                    wheelStage: (event) => console.log("wheelStage", event.event, event.preventSigmaDefault),
                    downStage: (event) => console.log("downStage", event.event, event.preventSigmaDefault),
                    // default mouse events
                    click: (event) => console.log("click", event.x, event.y),
                    doubleClick: (event) => console.log("doubleClick", event.x, event.y),
                    //wheel: (event) => console.log("wheel", event.x, event.y, event.delta),
                    rightClick: (event) => console.log("rightClick", event.x, event.y),
                    //mouseup: (event) => console.log("mouseup", event.x, event.y),
                    //mousedown: (event) => console.log("mousedown", event.x, event.y),
                    //mousemove: (event) => console.log("mousemove", event.x, event.y),
                    // default touch events
                    //touchup: (event) => console.log("touchup", event.touches),
                    //touchdown: (event) => console.log("touchdown", event.touches),
                    //touchmove: (event) => console.log("touchmove", event.touches),
                    // sigma kill
                    kill: () => console.log("kill"),
                    resize: () => console.log("resize"),
                    beforeRender: () => console.log("beforeRender"),
                    afterRender: () => console.log("afterRender"),
                    // sigma camera update
                    updated: (event) => console.log("updated", event.x, event.y, event.angle, event.ratio),
                });*/

                /* ========== END register events logging ========== */ 

                // set flag to prevent completely unnnecessary/unwanted redrawing of entire graph on every React render event
                alreadyRender = 1;
                
            })();
        }
    }, [loadGraph, registerEvents, panelData, setPanelData, colourMapList, setColourMapList]);

    return null;
};



const WibbleGraph = () => {

    const [ panelData, setPanelData ] = useState({
        note_date: "No Date Set",
        note_title: "Click on a node to view details",
        note_filetype: "No Filetype Set",
        note_id: "No ID Set",
        note_desc: "No Description Set",
        note_proj: "No Project Set",
        note_tags: "No Tags Set",
        note_class: "No Class Set",
        note_kp: "No KeyPairs Set",
        note_cust: "No Custom Set",
        note_filepath: "No Filepath Set",
        note_dd: "No Data Directory Set"
    });

    const [ colourMapList, setColourMapList ] = useState ("none");

    function ProjectsColourMapList(projectsColourMap) {
        var projHTML = ""; console.log("PROJECT: ", projectsColourMap.colourList);
        for(const [key, value] of projectsColourMap.colourList.entries()) {
            projHTML += "<p>" + key + "<span class='colour-swatch' style='background-color: " + value + "'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>";
            console.log("HELLO: ", projHTML);
        }
        return (<div>{parse(projHTML)}</div>);
    }

    function ProjectsCategoryPane ({colourList}) {
        return (<div className="node_projects_pane"><h2>Key</h2>
                <div className="project_colourmap"><ProjectsColourMapList colourList={projectsColourMap} /></div>
                </div>);
    }

    function genNoteToolbar(note_id, note_filepath, note_dd) {
    if(typeof note_id === 'undefined') { return (<></>); }
    return (
            <div className="toolbar_container">
            <div className="note_toolbar">
            <button id="copyNoteIdButton" data-param={"wibble search -GE --id " + note_id}  data-id={note_id} className="toolbar_button" onClick={() =>
                handleToolbarClick()} type="button"><img alt="" src="/img/clipboard-id.svg" height="20px"/>Copy Wibble Edit Command</button>
            <button id="copyNoteFilePathButton" data-param={note_filepath} data-id={note_id} className="toolbar_button" onClick={() =>
                handleToolbarClick()} type="button"><img alt="" src="/img/clipboard-path.svg" height="20px"/>Copy Note File Path</button>
            <button id="copyNoteDataDirButton" data-param={note_dd}  data-id={note_id} className="toolbar_button" onClick={() =>
                handleToolbarClick()} type="button"><img alt="" src="/img/clipboard-dd.svg" height="20px"/>Copy Note Data Directory Path</button>
            </div><div id="note_btn_status" className="button_click_status"><p>&nbsp;</p></div><hr/></div>);
    }
    
    
    function NodeInfoPane  ({note_date, note_title, note_filetype, note_id, note_desc, note_proj, note_tags, note_class, note_kp, note_cust, note_filepath, note_dd}) {

        let notePath = "";
        if(note_id === "No ID Set") {
            notePath = "";
        }
        else {
            notePath = "../wibble/search/" + note_id;
        }
            
        
        return (<div className="info_panel">
                <div className="node_info_pane"><h2>{note_title}</h2>
                <div className="node_info_details_pane">
                <div>{genNoteToolbar(note_id, note_filepath, note_dd)}</div>
                <p><span className="field"><b>Date:        </b></span><span className="field_value">{note_date}</span></p>
                <p><span className="field"><b>Filetype:    </b></span><span className="field_value">{note_filetype}</span></p>
                <p><span className="field"><b>ID:          </b></span><span className="field_value">
                <a target="_blank" rel="noreferrer" href={notePath}>{note_id}</a></span></p>
                <p><span className="field"><b>Description: </b></span><span className="field_value">{note_desc}</span></p>
                <p><span className="field"><b>Project:     </b></span><span className="field_value">{note_proj}</span></p>
                <p><span className="field"><b>Tags:        </b></span><span className="field_value">{note_tags}</span></p>
                <p><span className="field"><b>Class:       </b></span><span className="field_value">{note_class}</span></p>
                <p><span className="field"><b>KeyPairs:    </b></span><span className="field_value">{note_kp}</span></p>
                <p><span className="field"><b>Custom:      </b></span><span className="field_value">{note_cust}</span></p>
                </div>
                </div>
                </div>);
    };


    useEffect(() => {
        // register button event handling
        
        var noteIdButton;
        var noteFilePathButton;
        var noteDataDirButton;

        try {
            noteIdButton       = document.getElementById("copyNoteIdButton");
            noteFilePathButton = document.getElementById("copyNoteFilePathButton");
            noteDataDirButton  = document.getElementById("copyNoteDataDirButton");
        }
        catch { }
        
        if(noteIdButton !== null && noteIdButton !== undefined )             { noteIdButton.addEventListener("click", handleToolbarClick);   }
        if(noteFilePathButton !== null && noteFilePathButton !== undefined ) { noteFilePathButton.addEventListener("click", handleToolbarClick); }
        if(noteDataDirButton !== null && noteDataDirButton !== undefined )   { noteDataDirButton.addEventListener("click", handleToolbarClick);  }


        // cleanup
        return () => {
            if(noteIdButton !== null && noteIdButton !== undefined )             { noteIdButton.removeEventListener("click", handleToolbarClick); }
            if(noteFilePathButton !== null && noteFilePathButton !== undefined ) { noteFilePathButton.removeEventListener("click", handleToolbarClick); }
            if(noteDataDirButton !== null && noteDataDirButton !== undefined )   { noteDataDirButton.removeEventListener("click", handleToolbarClick); }
        }

    });

    return (<div className="graph_enclosing_parent">
        <div className="graph_content_area">
            <div className="graph_header_pane">
                <h1> Wibble Graph </h1>
                <span className="wibble_navigation">
                    <a href='/wibble'><img alt="menu" src='/img/wibble-menu-icon.svg' height='50px' /></a>
                    <a href='/wibble/search'><img alt="graph" src='/img/wibble-search-loupe.svg' height='50px' /></a>
                    <a href='/wibble/stats'><img alt="stats" src='/img/wibble-graph-icon.svg' height='50px' /></a>
                </span>
            </div>
            <SigmaContainer graph={MultiDirectedGraph} style={{ height: "1000px", width: "90%", background: "#212121" }} settings={{ labelColor: { color: "#44a2ff" } }}>
                <LoadGraph panelData={panelData} setPanelData={setPanelData} colourMapList={colourMapList} setColourMapList={setColourMapList} />

                <ControlsContainer position={"bottom-right"}>
                    <ZoomControl />
                    <FullScreenControl />
                </ControlsContainer>
                <ControlsContainer position={"top-right"}>
                    <SearchControl style={{ width: "400px" }} />
                </ControlsContainer>

            </SigmaContainer>
            <div className="info_container">
                <ProjectsCategoryPane />
                <NodeInfoPane {...panelData}></NodeInfoPane>
            </div>
        </div>
        <div className="graph_bottom_spacer_bg"></div>
    </div>
    );
};

export default WibbleGraph;
